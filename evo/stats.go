package evo


type Cluster struct {
	average  SocialGenome
	cells []*Cell
}

type Statistics struct {
	commonGenomes []*Cell
	world         *World
}

func NewStatistics(world *World) *Statistics {
	stats := new(Statistics)
	stats.world = world
	return stats
}

func (stats *Statistics) distance(cluster1, cluster2 []*Cell) int {
	minDist := SocialGenomeSize + 1
	for _, cell1 := range cluster1[:len(cluster1)] {
		for _, cell2 := range cluster2[:len(cluster2)] {
			dist := Similarity(cell1.socialGenome, cell2.socialGenome)
			if dist < minDist {
				minDist = dist
			}

		}
	}
	return minDist
}

func (stats *Statistics) CalculateCommonSocialGenomes() {
	/* populatonSize := len(stats.world.Population)

	clusters := make([][]*Cell, populatonSize)
	for i := range stats.world.Population {
		clusters[i] = []*Cell{stats.world.Population[i]}
	}

	for len(clusters) > 5 {
		minDist := SocialGenomeSize + 1
		var closest1, closest2 int
		for i, cluster1 := range clusters[:len(clusters)-1] {
			for j, cluster2 := range clusters[i+1:] {
				dist := stats.distance(cluster1, cluster2)
				if dist < minDist {
					minDist = dist
					closest1, closest2 = i, i+1+j
				}
			}
		}
		start := clusters[:closest1]
		middle := clusters[closest1+1 : closest2]
		end := append(middle, clusters[closest2+1:]...)
		clusters = append(start, end...)
	}*/

	clusters := make([]Cluster, len(stats.world.Population))
	for i, cell := range stats.world.Population {
		clusters[i] = Cluster{cell.socialGenome, []*Cell{stats.world.Population[i]}}
	}

	for i := 0; i < len(clusters); i++ {
		for j, cell := range stats.world.Population {
			if i == j {
				continue
			}
			// fmt.Printf("%b\n", clusters[i].hash)
			// fmt.Printf("%b\n", cell.socialGenomeBin)
			// fmt.Printf("%b\n", clusters[i].hash^cell.socialGenomeBin)
			// fmt.Println(bits.OnesCount(clusters[i].hash ^ cell.socialGenomeBin))
			if Similarity(clusters[i].average, cell.socialGenome) < int(float64(SocialGenomeSize)*0.3) {
				clusters[i].cells = append(clusters[i].cells, stats.world.Population[j])
			}
		}
	}

	biggestCluster := clusters[0]
	for _, cluster := range clusters[1:] {
		if len(cluster.cells) > len(biggestCluster.cells) {
			biggestCluster = cluster
		}
	}

	stats.commonGenomes = biggestCluster.cells
}

func (stats *Statistics) GetCommonSocialGenomes() []*Cell {
	return stats.commonGenomes
}
