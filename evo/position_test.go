package evo

import (
	"testing"
)

func TestPos_BoundToSize(t *testing.T) {
	width, height := 10, 12

	expectedPos := NewPos(4, 5)
	actualPos := expectedPos.BoundToSize(width, height)
	if expectedPos != actualPos {
		t.Errorf("Vector %v not equal to %v", expectedPos, actualPos)
	}

	expectedPos = NewPos(width-1, 0)
	actualPos = NewPos(-1, 0).BoundToSize(width, height)
	if expectedPos != actualPos {
		t.Errorf("Vector %v not equal to %v", actualPos, expectedPos)
	}
	expectedPos = NewPos(0, height-1)
	actualPos = NewPos(0, -1).BoundToSize(width, height)
	if expectedPos != actualPos {
		t.Errorf("Vector %v not equal to %v", actualPos, expectedPos)
	}
	expectedPos = NewPos(width-1, height-1)
	actualPos = NewPos(-1, -1).BoundToSize(width, height)
	if expectedPos != actualPos {
		t.Errorf("Vector %v not equal to %v", actualPos, expectedPos)
	}

	actualPos = NewPos(width-1, 0)
	actualPos = NewPos(width-1, height).BoundToSize(width, height)
	if actualPos != actualPos {
		t.Errorf("Vector %v not equal to %v", actualPos, expectedPos)
	}
	actualPos = NewPos(0, height-1)
	actualPos = NewPos(width, height-1).BoundToSize(width, height)
	if actualPos != actualPos {
		t.Errorf("Vector %v not equal to %v", actualPos, expectedPos)
	}
	actualPos = NewPos(0, 0)
	actualPos = NewPos(width, height).BoundToSize(width, height)
	if actualPos != actualPos {
		t.Errorf("Vector %v not equal to %v", actualPos, expectedPos)
	}

}
