package evo

import (
	"math"
	"math/rand"
)

type State byte

const (
	Alive     State = iota
	Reproduct State = iota
	Dead      State = iota
)

type Action byte

const (
	Stay Action = 0
	Move Action = 1
)

func (a Action) Invert() Action {
	if a == Stay {
		return Move
	} else {
		return Stay
	}
}

func (a Action) String() string {
	if a == Stay {
		return "Stay"
	} else {
		return "Move"
	}
}

type Strategy string

const (
	Cooperative Strategy = "Cooperative"
	Deceiving   Strategy = "Deceiving"
	Neutral     Strategy = "Neutral"
)

type Cell struct {
	Pos             Position
	state           State
	genome          []Action
	socialGenome    SocialGenome
	socialGenomeBin uint
	memory          SocialGenome

	health        int
	currentAction int
	interactions  int
}

func NewCell(position Position) *Cell {
	cell := new(Cell)
	cell.Pos = position
	cell.state = Alive
	cell.health = BasicCellHealth
	cell.genome = make([]Action, 0, GenomeSize)
	cell.socialGenome = NewSocialGenomeBin(SocialGenomeSize)
	cell.memory = NewSocialGenomeBin(GamesCountStored * 2)
	return cell
}

func (cell *Cell) InitGenome() {
	for i := 0; i < cap(cell.genome); i++ {
		cell.genome = append(cell.genome, Action(rand.Intn(2)))
	}
	cell.socialGenome.Init()
}

func (cell *Cell) NextAction(world *World) {
	if cell.genome[cell.currentAction] == Move {
		world.moveToFreeCell(cell)
	}

	cell.health--
	cell.currentAction = (cell.currentAction + 1) % len(cell.genome)
}

func (cell *Cell) UpdateState() {
	if cell.health < 0 {
		cell.health = 0
		cell.state = Dead
	} else if cell.health >= ReproductionCellHealth {
		cell.state = Reproduct
	}
	if cell.interactions > 0 {
		cell.interactions--
	}0
}

func (cell *Cell) GetState() State {
	return cell.state
}

func (cell *Cell) SetState(state State) {
	if state == Alive {
		cell.health = BasicCellHealth
	}
	cell.state = state
}

func (cell *Cell) Interact(partner *Cell) {
	var socialAction, partnerSocialAction SocialAction
	shift := uint(math.Pow(2, float64(GamesCountStored))) - 1

	if len(cell.memory.Get())/2 < GamesCountStored {
		socialAction = cell.socialGenome.Get()[cell.memory.ToNumber()]
	} else {
		socialAction = cell.socialGenome.Get()[cell.memory.ToNumber()+shift]
	}

	if len(partner.memory.Get())/2 < GamesCountStored {
		partnerSocialAction = partner.socialGenome.Get()[cell.memory.ToNumber()]
	} else {
		partnerSocialAction = partner.socialGenome.Get()[cell.memory.ToNumber()+shift]
	}

	if socialAction == partnerSocialAction {
		if socialAction == Cooperate {
			cell.health += 30
			partner.health += 30
		} else {
			cell.health -= 20
			partner.health -= 20
		}
	} else if partnerSocialAction == Deceive {
		partner.health += 40
		cell.health -= 30
	} else if socialAction == Deceive {
		cell.health += 40
		partner.health -= 30
	}

	cell.memory.Set(append(cell.memory.Get(), socialAction, partnerSocialAction))
	if len(cell.memory.Get())/2 >= GamesCountStored+1 {
		cell.memory.Set(cell.memory.Get()[2:])
	}

	partner.memory.Set(append(partner.memory.Get(), partnerSocialAction, socialAction))
	if len(partner.memory.Get())/2 >= GamesCountStored+1 {
		partner.memory.Set(cell.memory.Get()[2:])
	}

	cell.interactions = 2
	partner.interactions = 2
}

func (cell *Cell) CanInteract() bool {
	return cell.interactions < InteractionTimeout
}

func (cell *Cell) GetStrategy() Strategy {
	var score int
	for _, gene := range cell.socialGenome.Get() {
		if gene == Cooperate {
			score++
		} else if gene == Deceive {
			score--
		}
	}

	neutralRange := len(cell.genome) / 5
	if score > neutralRange {
		return Cooperative
	} else if score < -neutralRange {
		return Deceiving
	} else {
		return Neutral
	}
}

func (cell *Cell) GetHealth() int {
	return cell.health
}

func (cell *Cell) GetGenome() []Action {
	return cell.genome
}

func (cell *Cell) GetSocialGenome() SocialGenome {
	return cell.socialGenome
}

func (cell *Cell) GetMemory() SocialGenome {
	return cell.memory
}
