package evo

import (
	"math/rand"
	"time"
)

type World struct {
	Population    []*Cell
	Iterations    int
	Reproductions int

	Paused bool

	worldMap      [][]*Cell
	width, height int
}

func NewWorld() *World {
	world := new(World)
	world.width = WorldWidth
	world.height = WorldHeight

	world.worldMap = make([][]*Cell, world.height)
	for i := range world.worldMap {
		world.worldMap[i] = make([]*Cell, world.width)
	}
	return world
}

func (w *World) Populate() {
	w.Population = make([]*Cell, 0, PopulationSize)

	positions := make([]Position, w.width*w.height)
	for k, i := 0, 0; i < w.width; i++ {
		for j := 0; j < w.height; j++ {
			positions[k] = NewPos(i, j)
			k++
		}
	}
	for _, i := range rand.Perm(w.width * w.height)[:PopulationSize] {
		cell := NewCell(positions[i])
		cell.InitGenome()
		w.addCell(cell)
	}
}

func (w *World) Update() {
	if w.Paused {
		return
	}
	if len(w.Population) == 0 {
		w.Paused = true
		return
	}
	for i := 0; i < len(w.Population); i++ {
		cell := w.Population[i]

		if cell.GetState() == Dead {
			w.removeCell(i)
			continue
		}

		if cell.GetState() == Reproduct {
			neighbour := w.getCellNeighbour(cell)
			if neighbour != nil && neighbour.GetState() == Reproduct {
				w.reproduct(cell, neighbour)
			}
		}

		//
		//neighbour := w.getCellNeighbour(cell)
		//if neighbour != nil {
		//	w.reproduct(cell, neighbour)
		//}

		if cell.CanInteract() {
			neighbour := w.getCellNeighbour(cell)
			if neighbour != nil && neighbour.CanInteract() {
				cell.Interact(neighbour)
			}
		}

		cell.NextAction(w)
		cell.UpdateState()
	}
	w.Iterations++
}

func (w *World) Run() {
	for {
		w.Update()
	}
}

func (w *World) reproduct(parent1, parent2 *Cell) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	position, full := w.getCellEmptyNeighbourhood(parent1)
	if full {
		position, full = w.getCellEmptyNeighbourhood(parent2)
		if full {
			return
		}
	}

	child := NewCell(position)

	crossoverPoint := r.Intn(len(parent1.socialGenome.Get())-2) + 1
	child.socialGenome.Set(append(child.socialGenome.Get(), parent1.socialGenome.Get()[:crossoverPoint]...))
	child.socialGenome.Set(append(child.socialGenome.Get(), parent2.socialGenome.Get()[crossoverPoint:]...))

	crossoverPoint = r.Intn(len(parent1.genome)-2) + 1
	child.genome = append(child.genome, parent1.genome[:crossoverPoint]...)
	child.genome = append(child.genome, parent2.genome[crossoverPoint:]...)

	if r.Intn(100) < MutationRate {
		gene := r.Intn(len(child.socialGenome.Get()))
		child.socialGenome.SetGene(gene, ^child.socialGenome.Get()[gene]&1)
	}

	if r.Intn(100) < MutationRate {
		gene := r.Intn(len(child.genome))
		child.genome[gene] = child.genome[gene].Invert()
	}

	w.addCell(child)

	parent1.SetState(Alive)
	parent1.interactions = 2

	parent2.SetState(Alive)
	parent1.interactions = 2

	w.Reproductions++
}

func (w *World) addCell(cell *Cell) {
	x, y := cell.Pos.XY()
	w.worldMap[x][y] = cell
	w.Population = append(w.Population, cell)
}

func (w *World) removeCell(index int) {
	x, y := w.Population[index].Pos.XY()
	w.worldMap[x][y] = nil

	w.Population[index] = w.Population[len(w.Population)-1]
	w.Population[len(w.Population)-1] = nil
	w.Population = w.Population[:len(w.Population)-1]
}

func (w *World) getCellNeighbour(cell *Cell) *Cell {
	offsets := offsetsPerm[rand.Intn(len(offsetsPerm))]
	for _, offset := range offsets {
		neighbourPosition := cell.Pos.Add(offset)
		neighbourPosition = neighbourPosition.BoundToSize(w.width, w.height)
		if w.worldMap[neighbourPosition.X][neighbourPosition.Y] != nil {
			return w.worldMap[neighbourPosition.X][neighbourPosition.Y]
		}
	}
	return nil
}

func (w *World) getCellEmptyNeighbourhood(cell *Cell) (Position, bool) {
	offsets := offsetsPerm[rand.Intn(len(offsetsPerm))]
	for _, offset := range offsets {
		emptyPosition := cell.Pos.Add(offset)
		emptyPosition = emptyPosition.BoundToSize(WorldWidth, WorldHeight)
		if w.worldMap[emptyPosition.X][emptyPosition.Y] == nil {
			return emptyPosition, false
		}
	}
	return OriginPos, true
}

func (w *World) moveToFreeCell(cell *Cell) bool {
	newPosition, full := w.getCellEmptyNeighbourhood(cell)
	if !full {
		w.worldMap[cell.Pos.X][cell.Pos.Y] = nil
		cell.Pos = newPosition
		w.worldMap[cell.Pos.X][cell.Pos.Y] = cell
		return true
	}
	return false
}

func (w *World) GetCellByCoord(x, y int) *Cell {
	return w.worldMap[x][y]
}
