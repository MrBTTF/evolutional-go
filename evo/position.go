package evo

import "fmt"

type Position struct {
	X, Y int
}

var OriginPos = Position{0, 0}

func NewPos(x, y int) Position {
	return Position{x, y}
}

func (p Position) String() string {
	return fmt.Sprintf("Position(%v, %v)", p.X, p.Y)
}

func (p Position) XY() (x, y int) {
	return p.X, p.Y
}

func (p Position) Add(v Position) Position {
	return Position{
		p.X + v.X,
		p.Y + v.Y,
	}
}

func (p Position) Sub(v Position) Position {
	return Position{
		p.X - v.X,
		p.Y - v.Y,
	}
}

func (p Position) Scaled(c float64) Position {
	return Position{int(float64(p.X) * c), int(float64(p.Y) * c)}
}

func (p Position) BoundToSize(width, height int) Position {
	if p.X < 0 {
		p.X = width - 1
	} else if p.X >= width {
		p.X = 0
	}

	if p.Y < 0 {
		p.Y = height - 1
	} else if p.Y >= height {
		p.Y = 0
	}
	return p
}
