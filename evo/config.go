package evo


var (
	WorldWidth  int
	WorldHeight int

	PopulationSize   int
	GamesCountStored int
	GenomeSize       int
	SocialGenomeSize  int

	BasicCellHealth        int
	ReproductionCellHealth int

	InteractionTimeout = 1
	MutationRate       = 30 //percents
)

var offsets = [4]Position{
	NewPos(1, 0),
	NewPos(0, 1),
	NewPos(-1, 0),
	NewPos(0, -1),
}

var offsetsPerm [][]Position

func InitOffsetsPerm() {
	offsetsPerm = make([][]Position, 24)
	for i, indices := range permutations([]int{0,1,2,3}) {
		offsetsPerm[i] = make([]Position, 4)
		for j, index := range indices {
			offsetsPerm[i][j] = offsets[index]
		}
	}
}

func permutations(arr []int) [][]int{
    var helper func([]int, int)
    res := [][]int{}

    helper = func(arr []int, n int){
        if n == 1{
            tmp := make([]int, len(arr))
            copy(tmp, arr)
            res = append(res, tmp)
        } else {
            for i := 0; i < n; i++{
                helper(arr, n - 1)
                if n % 2 == 1{
                    tmp := arr[i]
                    arr[i] = arr[n - 1]
                    arr[n - 1] = tmp
                } else {
                    tmp := arr[0]
                    arr[0] = arr[n - 1]
                    arr[n - 1] = tmp
                }
            }
        }
    }
    helper(arr, len(arr))
    return res
}