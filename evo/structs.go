package evo

import (
	"fmt"
	"math"
	"math/rand"
)

type SocialAction byte

const (
	Cooperate SocialAction = 0
	Deceive   SocialAction = 1
)

func (sgb SocialAction) String() string {
	if sgb == Cooperate {
		return "Cooperate"
	}

	return "Deceive"
}

type SocialGenomeBin struct {
	value    []SocialAction
	binValue uint
}

func NewSocialGenomeBin(size int) *SocialGenomeBin {
	return &SocialGenomeBin{
		value: make([]SocialAction, 0, size),
	}
}

func (sgb *SocialGenomeBin) Init() {
	for i := 0; i < cap(sgb.value); i++ {
		sgb.value = append(sgb.value, SocialAction(rand.Intn(2)))
	}
	// sgb.binValue = sgb.toBin()
}

func (sgb SocialGenomeBin) Get() []SocialAction {
	return sgb.value
}

func (sgb *SocialGenomeBin) Set(value []SocialAction) {
	sgb.value = value
	// sgb.binValue = sgb.toBin()
}

func (sgb *SocialGenomeBin) SetGene(gene int, value SocialAction) {
	sgb.value[gene] = value
	// sgb.binValue = sgb.toBin()
}

func (sgb SocialGenomeBin) toBin() uint {
	var res uint

	fmt.Println(len(sgb.value))
	for i, action := range sgb.value {
		res += uint(math.Pow(2, float64(len(sgb.value)-1-i)) * float64(action))
	}
	return res
}

func (sgb SocialGenomeBin) ToNumber() uint {
	return sgb.binValue
}


func Similarity(sg1 SocialGenome, sg2 SocialGenome) int {
	distance := 0
	for i, action := range sg1.Get() {
		if action == sg2.Get()[i] {
			distance++
		}
	}
	return distance
}

//Similarity returns Hamming distance
// func Similarity(sa1 SocialGenome, sa2 SocialGenome) int {
// 	return bits.OnesCount(sa1.ToNumber() ^ sa2.ToNumber())
// }

type SocialGenome interface {
	Init()
	Get() []SocialAction
	Set(value []SocialAction)
	SetGene(gene int, value SocialAction)
	ToNumber() uint
}
