package evo

import (
	"math/rand"
	"os"
	"testing"
	"time"
)

func fillNeighbourhood(world *World, cell *Cell) {
	for _, offset := range offsets {
		pos := cell.Pos.Add(offset)
		world.addCell(NewCell(pos))
	}
}

func TestMain(m *testing.M) {
	WorldWidth = 10
	WorldHeight = 10
	PopulationSize = 20
	BasicCellHealth = 10
	SocialGenomeSize = 10
	os.Exit(m.Run())
}

func TestWorld_Populate(t *testing.T) {
	world := NewWorld()
	world.Populate()
	if len(world.Population) != PopulationSize {
		t.Errorf("Population size is %d, expected %d", len(world.Population), PopulationSize)
	}
}

func Test_addCell(t *testing.T) {
	world := NewWorld()

	x, y := rand.Intn(world.width), rand.Intn(world.height)
	cell := NewCell(NewPos(x, y))

	world.addCell(cell)
	if world.Population[0] != cell {
		t.Error("Cell is not in Population slice!")
	}
	if world.worldMap[x][y] != cell {
		t.Error("Cell is not in worldMap!")
	}
}

func Test_removeCell(t *testing.T) {
	world := NewWorld()

	x, y := rand.Intn(world.width), rand.Intn(world.height)
	cell := NewCell(NewPos(x, y))
	world.addCell(cell)

	world.removeCell(0)
	if len(world.Population) != 0 {
		t.Error("Cell shouldn't be in Population slice!")
	}
	if world.worldMap[x][y] == cell {
		t.Error("Cell shouldn't be in worldMap!")
	}
}

func TestWorld_reproduct(t *testing.T) {
	world := NewWorld()
	x, y := 5, 5
	cell1 := NewCell(NewPos(x, y))
	world.addCell(cell1)
	cell1.InitGenome()

	cell2 := NewCell(NewPos(x+1, y))
	world.addCell(cell2)
	cell2.InitGenome()

	world.reproduct(cell1, cell2)
	child := world.Population[2]

	positionCorrect := false

	for _, offset := range offsets {
		if child.Pos == cell1.Pos.Add(offset) {
			positionCorrect = true
			break
		}
	}
	if !positionCorrect {
		t.Error("Child cell not found in neighbourhood")
	}
}

func TestWorld_getCellEmptyNeighbourhood(t *testing.T) {
	world := NewWorld()
	x, y := 5, 5
	cell := NewCell(NewPos(x, y))
	for _, offset := range offsets[1:] {
		pos := cell.Pos.Add(offset)
		world.addCell(NewCell(pos))
	}

	emptyPosition, full := world.getCellEmptyNeighbourhood(cell)
	if full {
		t.Error("Expected an empty position next to the cell")
	}
	expectedPos := cell.Pos.Add(offsets[0])
	if expectedPos != emptyPosition {
		t.Errorf("Wrong empty position %v for the cell, expected %v", expectedPos, emptyPosition)
	}

	pos := cell.Pos.Add(offsets[0])
	world.addCell(NewCell(pos))
	emptyPosition, full = world.getCellEmptyNeighbourhood(cell)
	if !full {
		t.Error("Expected no empty position next to the cell")
	}
}

func TestWorld_getCellNeighbour(t *testing.T) {
	world := NewWorld()
	x, y := 5, 5
	cell := NewCell(NewPos(x, y))

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	offset := offsets[r.Intn(len(offsets))]
	neighbour := world.getCellNeighbour(cell)
	if neighbour != nil {
		t.Error("Expected no neighbour next to the cell")
	}

	pos := cell.Pos.Add(offset)
	world.addCell(NewCell(pos))

	neighbour = world.getCellNeighbour(cell)
	if neighbour == nil {
		t.Error("Expected a neighbour next to the cell")
	}
	if neighbour.Pos != pos {
	}
}

func TestWorld_MoveToFreeCell(t *testing.T) {
	world := NewWorld()
	pos := NewPos(5, 5)
	cell := NewCell(pos)
	moved := world.moveToFreeCell(cell)

	if cell.Pos == pos {
		t.Error("Cell's position must be different")
	}
	if !moved {
		t.Error("Cell not moved")
	}

	oldPos := cell.Pos
	fillNeighbourhood(world, cell)
	moved = world.moveToFreeCell(cell)
	if cell.Pos != oldPos {
		t.Errorf("Actual position %v, expected %v", cell.Pos, oldPos)
	}
	if moved {
		t.Error("Cell shouldn't have moved")
	}
}

func TestCell_NextAction(t *testing.T) {
	world := NewWorld()
	x, y := 5, 5
	cell := NewCell(NewPos(x, y))
	cell.genome = []Action{Move, Stay, Move}
	oldHealth := cell.health
	cell.NextAction(world)

	if oldHealth-1 != cell.health {
		t.Errorf("Actual health %v, expected %v", cell.health, oldHealth)
	}

	oldHealth = cell.health
	cell.NextAction(world)
	if oldHealth == cell.health {
		t.Errorf("Actual health %v, expected %v", cell.health, oldHealth)
	}

	fillNeighbourhood(world, cell)
	oldHealth = cell.health
	cell.NextAction(world)
	if oldHealth == cell.health {
		t.Errorf("Actual health %v, expected %v", cell.health, oldHealth)
	}

}
