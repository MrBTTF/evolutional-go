package ui

import (
	"fmt"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"gitlab.com/MrBTTF/evolutional-go/evo"
	"golang.org/x/image/colornames"
)

const (
	borderSize = 2
)

var CellSize float64

var (
	neutralColor = pixel.RGB(0, 194, 225).Scaled(1.0 / 255)
	redColor     = pixel.RGB(255, 0, 93).Scaled(1.0 / 255)
	greenColor   = pixel.RGB(106, 255, 0).Scaled(1.0 / 255)
	yellowColor  = pixel.RGB(255, 255, 0).Scaled(1.0 / 255)
)

type DisplayMode string

var (
	None DisplayMode = "None"
	MostCommon DisplayMode = "MostCommon"
	Strategy   DisplayMode = "Strategy"
)

type WorldDrawer struct {
	width, height float64
	displayMode   DisplayMode

	world      *evo.World
	statistics *evo.Statistics
}

func NewWorldDrawer(world *evo.World, width, height float64, statistics *evo.Statistics) *WorldDrawer {
	worldDrawer := new(WorldDrawer)
	worldDrawer.width = width
	worldDrawer.height = height
	worldDrawer.world = world
	worldDrawer.displayMode = Strategy
	worldDrawer.statistics = statistics
	return worldDrawer
}

func (wd *WorldDrawer) Draw(t pixel.Target) {
	wd.drawField(t)
	wd.drawPopulation(t)
}

func (wd *WorldDrawer) drawCell(t pixel.Target, cell *evo.Cell, color pixel.RGBA) {
	imd := imdraw.New(nil)
	imd.Color = color

	cellPosition := PosToVec(cell.Pos)
	imd.Push(cellPosition.Scaled(CellSize))
	imd.Push(cellPosition.Scaled(CellSize).Add(pixel.V(CellSize, CellSize)))
	imd.Rectangle(0)
	imd.Color = colornames.White
	imd.Push(cellPosition.Scaled(CellSize).Add(pixel.V(borderSize, borderSize)))
	imd.Push(cellPosition.Scaled(CellSize).Add(pixel.V(CellSize, CellSize)).Add(pixel.V(-borderSize, -borderSize)))
	imd.Rectangle(0)
	imd.Draw(t)
}

func (wd *WorldDrawer) drawPopulation(t pixel.Target) {
	for i, cell := range wd.world.Population {
		if cell != nil && cell.GetState() != evo.Dead {
			cellColor := neutralColor
			if wd.displayMode == Strategy {
				cellColor = getColorByStrategy(cell.GetStrategy())
			} else if wd.displayMode == MostCommon {
				cells := wd.statistics.GetCommonSocialGenomes()
				for j := 0; j < len(cells); j++ {
					if cells[j] == wd.world.Population[i] {
						cellColor = redColor
					}
				}
			}

			wd.drawCell(t, cell, cellColor)
		}
	}
}

func (wd *WorldDrawer) drawField(t pixel.Target) {
	field := imdraw.New(nil)
	field.Color = colornames.Black
	field.Push(pixel.V(0, 0))
	field.Push(pixel.V(wd.width, wd.height))
	field.Rectangle(0)
	field.Draw(t)
}

func getColorByStrategy(strategy evo.Strategy) pixel.RGBA {
	switch strategy {
	case evo.Neutral:
		return neutralColor
	case evo.Cooperative:
		return greenColor
	case evo.Deceiving:
		return redColor
	default:
		panic(fmt.Errorf("no such color: %v", strategy))
	}
}
