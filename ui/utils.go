package ui

import (
	"github.com/faiface/pixel"
	"gitlab.com/MrBTTF/evolutional-go/evo"
)

func PosToVec(position evo.Position) pixel.Vec {
	x, y := position.XY()
	return pixel.V(float64(x), float64(y))
}
