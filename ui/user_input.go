package ui

import (
	"fmt"

	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/MrBTTF/evolutional-go/evo"
)

type InputManager struct {
	Paused bool

	window      *pixelgl.Window
	worldDrawer *WorldDrawer
	world       *evo.World
}

func NewInputManager(window *pixelgl.Window, worldDrawer *WorldDrawer, world *evo.World) *InputManager {
	inputManager := new(InputManager)
	inputManager.window = window
	inputManager.worldDrawer = worldDrawer
	inputManager.world = world
	inputManager.Paused = false
	return inputManager
}

func (im *InputManager) Update() {
	if im.window.JustPressed(pixelgl.KeyP) {
		im.Paused = !im.Paused
		im.world.Paused = im.Paused
	} else if im.window.JustPressed(pixelgl.MouseButtonLeft) {
		mousePos := im.window.MousePosition()
		if mousePos.X > im.worldDrawer.width || mousePos.Y > im.worldDrawer.height {
			return
		}

		x, y := mousePos.Scaled(1.0 / CellSize).XY()

		cell := im.world.GetCellByCoord(int(x), int(y))
		if cell == nil {
			return
		}

		fmt.Println("genome:")
		fmt.Println(cell.GetGenome())
		fmt.Println("socialGenome:")
		fmt.Println(cell.GetSocialGenome())
		fmt.Println("memory:")
		fmt.Println(cell.GetMemory())
	} else if im.window.JustPressed(pixelgl.Key1) {
		im.worldDrawer.displayMode = None
	} else if im.window.JustPressed(pixelgl.Key2) {
		im.worldDrawer.displayMode = Strategy
	} else if im.window.JustPressed(pixelgl.Key3) {
		im.worldDrawer.statistics.CalculateCommonSocialGenomes()
		im.worldDrawer.displayMode = MostCommon
	}

}
