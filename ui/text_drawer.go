package ui

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/text"
	"github.com/golang/freetype/truetype"
	"gitlab.com/MrBTTF/evolutional-go/evo"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font"
)

func loadTTF(path string, size float64) (font.Face, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	font, err := truetype.Parse(bytes)
	if err != nil {
		return nil, err
	}

	return truetype.NewFace(font, &truetype.Options{
		Size:              size,
		GlyphCacheEntries: 1,
	}), nil
}

type InfoDrawer struct {
	origin     pixel.Vec
	atlas      *text.Atlas
	world      *evo.World
	statistics *evo.Statistics
}

func NewInfoDrawer(origin pixel.Vec, world *evo.World, statistics *evo.Statistics) *InfoDrawer {
	infoDrawer := new(InfoDrawer)
	infoDrawer.origin = origin
	infoDrawer.world = world
	infoDrawer.statistics = statistics

	face, err := loadTTF("ui/roboto.ttf", 30)
	if err != nil {
		panic(err)
	}
	infoDrawer.atlas = text.NewAtlas(face, text.ASCII)

	return infoDrawer
}

func (id *InfoDrawer) Draw(t pixel.Target) {
	basicText := text.New(id.origin, id.atlas)
	basicText.Color = colornames.Black
	fmt.Fprintf(basicText, "Iterations: %d\n", id.world.Iterations)
	fmt.Fprintf(basicText, "Population size: %d\n", len(id.world.Population))
	fmt.Fprintf(basicText, "Reproductions: %d\n", id.world.Reproductions)

	maxHealth := 0
	minHealth := math.MaxInt64
	commonCellsCount := 0
	for i, cell := range id.world.Population {
		if cell == nil {
			continue
		}
		if cell.GetHealth() > maxHealth {
			maxHealth = cell.GetHealth()
		}
		if cell.GetHealth() < minHealth {
			minHealth = cell.GetHealth()
		}	

		cells := id.statistics.GetCommonSocialGenomes()
		for j := 0; j < len(cells); j++ {
			if cells[j] == id.world.Population[i] {
				commonCellsCount++
			}
		}

	}

	fmt.Fprintf(basicText, "Max health: %d\n", maxHealth)
	fmt.Fprintf(basicText, "Min health: %d\n", minHealth)

	fmt.Fprintf(basicText, "Common genome: %.2f%%\n", float64(commonCellsCount)/float64(len(id.world.Population))*100)

	basicText.Draw(t, pixel.IM)
}
