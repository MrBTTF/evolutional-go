package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/MrBTTF/evolutional-go/evo"
	"gitlab.com/MrBTTF/evolutional-go/ui"
	"golang.org/x/image/colornames"
)

const (
	WindowWidth  = 1000
	WindowHeight = 600
)

func InitEvo() {
	ui.CellSize = 15

	evo.WorldWidth = WindowHeight / int(ui.CellSize)
	evo.WorldHeight = WindowHeight / int(ui.CellSize)
	evo.PopulationSize = 300
	evo.GamesCountStored = 2
	evo.SocialGenomeSize = int(math.Pow(2, float64(evo.GamesCountStored))) - 1 + int(math.Pow(2, 2*float64(evo.GamesCountStored)))
	fmt.Printf("%d\n", evo.SocialGenomeSize)
	evo.GenomeSize = 10
	evo.MutationRate = 30
	evo.BasicCellHealth = 100
	evo.ReproductionCellHealth = 200 //int(float64(evo.BasicCellHealth) * 1.1)
	evo.InitOffsetsPerm()
}

func InitWindow() *pixelgl.Window {
	cfg := pixelgl.WindowConfig{
		Title:  "Evolutional",
		Bounds: pixel.R(0, 0, WindowWidth, WindowHeight),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	return win
}

func run() {
	rand.Seed(time.Now().UnixNano())

	window := InitWindow()
	InitEvo()

	world := evo.NewWorld()
	world.Populate()
	go world.Run()

	stats := evo.NewStatistics(world)
	worldDrawer := ui.NewWorldDrawer(world, WindowHeight, WindowHeight, stats)
	infoPanel := ui.NewInfoDrawer(pixel.V(WindowHeight, WindowHeight-30), world, stats)
	inputManager := ui.NewInputManager(window, worldDrawer, world)

	fps := time.Tick(time.Second / 60)
	for !window.Closed() {
		window.Clear(colornames.White)

		inputManager.Update()

		if inputManager.Paused {
			worldDrawer.Draw(window)
			infoPanel.Draw(window)
			window.Update()
			continue
		}

		// world.Update()
		worldDrawer.Draw(window)
		infoPanel.Draw(window)

		window.Update()

		<-fps
	}
}

func main() {
	pixelgl.Run(run)
}
