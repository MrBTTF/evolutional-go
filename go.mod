module gitlab.com/MrBTTF/evolutional-go

require (
	github.com/faiface/glhf v0.0.0-20181018222622-82a6317ac380 // indirect
	github.com/faiface/mainthread v0.0.0-20171120011319-8b78f0a41ae3 // indirect
	github.com/faiface/pixel v0.8.0
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2 // indirect
	github.com/go-gl/glfw v0.0.0-20190409004039-e6da0acd62b1 // indirect
	github.com/go-gl/mathgl v0.0.0-20180804195959-cdf14b6b8f8a // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/pkg/errors v0.8.1 // indirect
	golang.org/x/image v0.0.0-20190227222117-0694c2d4d067
)
